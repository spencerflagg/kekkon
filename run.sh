# echo -e "\r\n\r\n\e[44m *** Removing Code Directory *** \e[44m\r\n"
# rm -r /usr/src/kekkon
# echo "\r\n\r\n\e[44m *** Making Code Directory *** \e[44m\r\n"
# mkdir -p /usr/src/kekkon
cd /usr/src
# echo "\r\n\r\n\e[44m *** Getting Code from Gitea *** \e[44m\r\n"
# git clone --depth 1 https://gitea.flylocal.us/spencer/kekkon.git
cd kekkon
echo "\r\n\r\n\e[44m *** Killing Docker Containers *** \e[44m\r\n"
sudo docker kill $(docker ps -q)
echo "\r\n\r\n\e[44m *** Pruning Old Docker Images *** \e[44m\r\n"
sudo docker system prune
echo "\r\n\r\n\e[44m *** Building New Docker Image *** \e[44m\r\n"
sudo docker build -t kekkon .
echo "\r\n\r\n\e[44m *** Running Docker Image *** \e[44m\r\n"
sudo docker run -it -p 3000:3000 kekkon